const mongoose = require('mongoose')

const codeSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    default: 'Untitled'
  },
  url: {
    type: String,
    required: true,
    unique: true
  },
  html: String,
  css: String,
  js: String,
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
}, {
  versionKey: false,
  timestamps: true
})

module.exports = mongoose.model('Code', codeSchema)
