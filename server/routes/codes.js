const router = require('@koa/router')()
const Code = require('../models/Code')
const { getUserId } = require('../utils')

router.prefix('/codes')

router.get('/', async (ctx) => {
  const codes = await Code.find().sort({ updatedAt: -1 })
  ctx.success(codes)
})

router.get('/:url', async (ctx) => {
  const { url } = ctx.params
  const code = await Code.findOne({ url })
  ctx.success(code)
})

router.post('/', async (ctx) => {
  const { name, url, html, css, js } = ctx.request.body
  const code = await Code.findOne({ url })
  if (!code) {
    await Code.create({
      name,
      url,
      html,
      css,
      js,
      author: getUserId(ctx)
    })
  } else {
    await code.updateOne({ name, html, css, js })
  }
  ctx.success('')
})

router.delete('/:id', async (ctx) => {
  const { id } = ctx.params
  await Code.findByIdAndDelete(id)
  ctx.success('')
})

module.exports = router
