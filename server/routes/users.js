const router = require('@koa/router')()
const crypto = require('crypto-js')
const User = require('../models/User')
const jwt = require('jsonwebtoken')

router.prefix('/users')

router.post('/register', async ctx => {
  const { username, password, email } = ctx.request.body
  const encryptPassword = crypto.AES.encrypt(password, 'SECRET').toString()
  await User.create({username, password: encryptPassword, email})
  ctx.success('')
})

router.post('/login', async ctx => {
  const {username, password} = ctx.request.body
  const user = await User.findOne({username}).lean()
  if (!user) return ctx.fail('用户不存在')
  const decodePassword = crypto.AES.decrypt(user.password, 'SECRET').toString(crypto.enc.Utf8)
  if (password !== decodePassword) return ctx.fail('密码输入错误')
  const token = jwt.sign(user, 'SECRET', {expiresIn: '7d'})
  Reflect.deleteProperty(user, 'password')
  ctx.success({
    user,
    token
  })
})

module.exports = router
