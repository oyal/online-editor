const Koa = require('koa')
const app = new Koa()
const cors = require('@koa/cors')
const { koaBody } = require('koa-body')
const jwt = require('koa-jwt')

const usersRouter = require('./routes/users')
const codesRouter = require('./routes/codes')

require('./config/db.js')

// error handler
app.use(async (ctx, next) => {
  try {
    await next()
  } catch (err) {
    ctx.status = err.statusCode || err.status || 500
    ctx.body = {
      code: err.status,
      msg: err.message
    }
    ctx.app.emit('error', err, ctx)
  }
})

// 404
app.use(async (ctx, next) => {
  await next()
  if (ctx.status === 404) {
    ctx.body = {
      code: 404,
      message: 'Not Found'
    }
  }
})

// middlewares
app.use(cors())
app.use(koaBody())
app.use(
  jwt({ secret: 'SECRET' }).unless({
    path: [/^\/users\/login/, /^\/users\/register/]
  })
)

app.use(async (ctx, next) => {
  ctx.success = (data, msg) => {
    ctx.body = {
      code: 200,
      msg: msg || 'success',
      data
    }
  }
  ctx.fail = (msg) => {
    ctx.body = {
      code: 400,
      msg: msg || 'success'
    }
  }
  await next()
})

// routes
app.use(usersRouter.routes()).use(usersRouter.allowedMethods())
app.use(codesRouter.routes()).use(codesRouter.allowedMethods())

// error logger
app.on('error', (err) => {
  console.log('server error', err.message)
})

app.listen(3000, () => {
  console.log('Server is running on port 3000')
})
