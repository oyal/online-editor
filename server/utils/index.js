const jwt = require("jsonwebtoken")

exports.getUserId = (ctx) => {
    const token = ctx.get('Authorization').split(' ').pop()
    const decode = jwt.verify(token, 'SECRET')
    return decode._id
}
