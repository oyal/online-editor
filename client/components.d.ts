// generated by unplugin-vue-components
// We suggest you to commit this file into source control
// Read more: https://github.com/vuejs/core/pull/3399
import '@vue/runtime-core'

export {}

declare module '@vue/runtime-core' {
  export interface GlobalComponents {
    MonacoEditor: typeof import('./src/components/MonacoEditor/index.vue')['default']
    NButton: typeof import('naive-ui')['NButton']
    NCard: typeof import('naive-ui')['NCard']
    NConfigProvider: typeof import('naive-ui')['NConfigProvider']
    NDialogProvider: typeof import('naive-ui')['NDialogProvider']
    NDivider: typeof import('naive-ui')['NDivider']
    NDropdown: typeof import('naive-ui')['NDropdown']
    NForm: typeof import('naive-ui')['NForm']
    NFormItem: typeof import('naive-ui')['NFormItem']
    NGrid: typeof import('naive-ui')['NGrid']
    NGridItem: typeof import('naive-ui')['NGridItem']
    NIcon: typeof import('naive-ui')['NIcon']
    NInput: typeof import('naive-ui')['NInput']
    NLayout: typeof import('naive-ui')['NLayout']
    NLayoutContent: typeof import('naive-ui')['NLayoutContent']
    NLayoutHeader: typeof import('naive-ui')['NLayoutHeader']
    NLayoutSider: typeof import('naive-ui')['NLayoutSider']
    NMenu: typeof import('naive-ui')['NMenu']
    NMessageProvider: typeof import('naive-ui')['NMessageProvider']
    NModal: typeof import('naive-ui')['NModal']
    NPopover: typeof import('naive-ui')['NPopover']
    RouterLink: typeof import('vue-router')['RouterLink']
    RouterView: typeof import('vue-router')['RouterView']
    SvgIcon: typeof import('./src/components/SvgIcon/index.vue')['default']
  }
}
