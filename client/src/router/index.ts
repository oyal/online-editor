import { createRouter, createWebHistory, type RouteRecordRaw } from 'vue-router'

const routes: RouteRecordRaw[] = [
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/login/index.vue')
  },
  {
    path: '/',
    component: () => import('@/views/projects/index.vue'),
    redirect: '/projects',
    children: [
      {
        path: 'projects',
        name: 'projects',
        component: () => import('@/views/projects/Projects.vue')
      }
    ]
  },
  {
    path: '/settings',
    component: () => import('@/views/settings/index.vue'),
    redirect: '/settings/profile',
    children: [
      {
        path: 'profile',
        name: 'profile',
        component: () => import('@/views/profile/index.vue')
      },
      {
        path: 'preferences',
        name: 'preferences',
        component: () => import('@/views/preferences/index.vue')
      }
    ]
  },
  {
    path: '/editor',
    component: () => import('@/views/editor/index.vue'),
    children: [
      {
        path: ':id',
        name: 'editor',
        component: () => import('@/views/editor/Editor.vue')
      }
    ]
  }
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
})

export default router
