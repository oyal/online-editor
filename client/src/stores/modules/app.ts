import { defineStore } from 'pinia'

const useAppStore = defineStore('app', {
  state: () => ({
    isDark: true
  }),
  actions: {
    toggleTheme() {
      this.isDark = !this.isDark
    }
  },
  persist: true
})

export default useAppStore
