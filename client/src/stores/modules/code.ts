import { defineStore } from 'pinia'

const useCodeStore = defineStore('code', {
  state: () => ({
    name: '',
    html: '',
    css: '',
    js: ''
  }),
  persist: {
    key: 'code',
    storage: sessionStorage
  }
})

export default useCodeStore
