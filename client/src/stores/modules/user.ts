import { defineStore } from 'pinia'

const useUserStore = defineStore('user', {
  state: () => ({
    userInfo: <any>{},
    token: ''
  }),
  actions: {
    setUserInfo(userInfo: any) {
      this.userInfo = userInfo
    },
    setToken(token: string) {
      this.token = token
    }
  },
  persist: true
})

export default useUserStore
