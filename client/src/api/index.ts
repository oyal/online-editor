import request from '@/utils/request'

export const login = (data: any) => {
  return request({
    url: '/users/login',
    method: 'post',
    data
  })
}

export const getAllCodes = () => {
  return request({
    url: '/codes',
    method: 'get'
  })
}

export const getCodeByUrl = (url: string) => {
  return request({
    url: `/codes/${url}`,
    method: 'get'
  })
}

export const saveCode = (data: any) => {
  return request({
    url: '/codes',
    method: 'post',
    data
  })
}

export const deleteCode = (id: string) => {
  return request({
    url: `/codes/${id}`,
    method: 'delete'
  })
}
