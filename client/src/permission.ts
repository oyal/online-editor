import router from '@/router'
import useUserStore from '@/stores/modules/user'
import stores from '@/stores'

router.beforeEach((to, from, next) => {
  const userStore = useUserStore(stores)
  if (to.path === '/login') {
    next()
  } else {
    if (userStore.token) {
      next()
    } else {
      next('/login')
    }
  }
})
