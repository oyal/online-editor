import axios from 'axios'
import useUserStore from '@/stores/modules/user'
import stores from '@/stores'
import { createDiscreteApi } from 'naive-ui'
import router from '@/router'

const { message } = createDiscreteApi(['message'])

const server = axios.create({
  baseURL: import.meta.env.VITE_API_URL,
  timeout: 10000
})

server.interceptors.request.use(
  (request) => {
    const userStore = useUserStore(stores)
    if (userStore.token) {
      request.headers.Authorization = `Bearer ${userStore.token}`
    }
    return request
  },
  (error) => {
    message.error(error.message)
    return Promise.reject(error)
  }
)

server.interceptors.response.use(
  (response) => {
    const data = response.data
    if (data.code !== 200) {
      message.error(data.msg)
    }
    return data
  },
  (error) => {
    if (error.response?.data.code === 401) {
      message.error('登录过期，请重新登录')
      const userStore = useUserStore(stores)
      userStore.$reset()
      router.push('/login')
      return
    }
    message.error(error.message)
    return Promise.reject(error)
  }
)

export default server
