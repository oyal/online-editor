import { h, computed } from 'vue'
import { NIcon } from 'naive-ui'
import SvgIcon from '@/components/SvgIcon/index.vue'
import { useWindowSize } from '@vueuse/core'

export const renderIcon = (name: string) => {
  return () => h(NIcon, null, { default: () => h(SvgIcon, { name }) })
}

const { width } = useWindowSize()
export const isMobileTerminal = computed(() => {
  return width.value < 768
})
