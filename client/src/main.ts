import { createApp } from 'vue'
import store from '@/stores'

import App from './App.vue'
import router from './router'
import SvgIcon from '@/components/SvgIcon/index.vue'
import 'virtual:svg-icons-register'

import './assets/index.css'

import './permission'

const meta = document.createElement('meta')
meta.name = 'naive-ui-style'
document.head.appendChild(meta)

const app = createApp(App)

app.use(store)
app.use(router)

app.component('SvgIcon', SvgIcon)

app.mount('#app')
